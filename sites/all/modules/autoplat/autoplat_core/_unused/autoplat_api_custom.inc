<?php

// This API is Project Specific

// render a button
function __button($path, $string = 'Continue') {
  $output = l($string . ' &nbsp;&nbsp;<i class="fa fa-angle-right"></i>', $path, array('attributes'=>array('class'=>'btn'), 'html' => true));
  return $output;
}

// full path to theme

/**
// is this a duplicate of __renderMoreLink??
function __button($title = 'See more', $path = NULL, $options = array()) {
  $output = '<a class="btn" href="' . url($path, $options) . '">' . $title . ' &nbsp;&nbsp;<i class="fa fa-angle-right"></i></a>';
  
  return $output;
}
*/

/**
function __button($title = 'See more', $path = NULL, $options = array()) {
  $output = '<a class="btn" href="' . url($path, $options) . '">' . $title . ' &nbsp;&nbsp;<i class="fa fa-angle-right"></i></a>';
  
  return $output;
}
*/

// render a menu for Before & After content
function __beforeafterMenu($nodeType) {
  $nid = ($node = menu_get_object()) ? $node->nid : NULL;

  /**
  * Create Archive Menu (array)
  */
  
  // get requested year and month
  $query = drupal_get_query_parameters();
  $requestedType = isset($query['type']) ? $query['type'] : NULL;
  $requestedGrafts = isset($query['grafts']) ? $query['grafts'] : NULL;
  
  $menu = array(
    'fue'=>'FUE (No Linear Scar)',
    'fut'=>'FUT (Traditional)',
    ''=>'All',
  );
  
  $output = '<div class="region menu">';
  $output .= '<h3>Transplant Type</h3>';
  $output .= '<ul>';
  foreach ($menu as $key=>$val) {
    $options = array('array' => array());
    if ($key) { $options['query']['type'] = $key; }
    if ($requestedGrafts) { $options['query']['grafts'] = $requestedGrafts; }
    $output .= $val === reset($menu) ? '<li class="first">' : '<li>';
    $output .= $key == $requestedType ? '<a class="active"' : '<a';
    $output .= ' href="' . url('node/' . $nid, $options) . '">' . $val . '</a></li>';
  }
  $output .= '</ul>';
  $output .= '</div>';
  
  $menu = array(
    '0-1500'=>'0 - 1500',
    '1500-3000'=>'1500 - 3000',
    '3000-4500'=>'3000 - 4500',
    '4500-99999'=>'4500+',
    ''=>'All',
  );
  
  $output .= '<div class="region menu">';
  $output .= '<h3>Grafts Count</h3>';
  $output .= '<ul>';
  foreach ($menu as $key=>$val) {
    $options = array('array' => array());
    if ($key) { $options['query']['grafts'] = $key; }
    if ($requestedType) { $options['query']['type'] = $requestedType; }
    $output .= $val === reset($menu) ? '<li class="first">' : '<li>';
    $output .= $key == $requestedGrafts ? '<a class="active"' : '<a';
    $output .= ' href="' . url('node/' . $nid, $options) . '">' . $val . '</a></li>';
  }
  $output .= '</ul>';
  $output .= '</div>';
  
  return $output;
}


// child of __beforeafterMenu. gets Nids based on parent menu.
function __beforeafterMenu_getNids($nodeType) {
  $nid = ($node = menu_get_object()) ? $node->nid : NULL;

  /**
  * Create Archive Menu (array)
  */
  
  // get requested year and month
  $query = drupal_get_query_parameters();
  $requestedType = isset($query['type']) ? $query['type'] : NULL;
  if (isset($query['grafts'])) {
    $requestedGrafts = explode('-', $query['grafts']);
    if (count($requestedGrafts) != 2 && is_numeric($requestedGrafts[0]) && is_numeric($requestedGrafts[1])) {
      $requestedGrafts = NULL;
    }
  } else {
    $requestedGrafts = NULL;
  }
  
  //load older nids
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
  ->propertyCondition('type', $nodeType)
  ->propertyCondition('status', 1)
  ->propertyOrderBy('created', 'DESC');
  //exclude existing nids
  //process query

  if ($requestedType) {
    $query->fieldCondition('field_proceduretype', 'value', $requestedType);
  }
  
  if (isset($requestedGrafts)) {
    $query->fieldCondition('field_grafts', 'value', $requestedGrafts[0], '>');
    $query->fieldCondition('field_grafts', 'value', $requestedGrafts[1], '<');
  }
  
  $result = $query->execute();
  $nids = array();
  if (isset($result['node'])) {
    $nodes = $result['node'];
    foreach ($nodes as $node) {
      $nids[] = $node->nid;
    }  
  }
  
  return $nids;
  
  //return $output;
}

// fb like button
function __renderSocialFb($current_url) {
  $current_url_encoded = urlencode($current_url);
  $output = '<iframe src="//www.facebook.com/plugins/like.php?href=' . $current_url_encoded . '&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>';
  return $output;
}

// renders an ad
function __adRegion($options = NULL) {
  if (!$options) {
    $options = array(
      'type' => 'e1b13',
      'limit' => 1,
      'sort' => 'random',
    );
  }
  $nids = __getNids($options);
  $index = __grabAll($nids[0]);
  $output = $index->link ? '<a href="' . $index->link . '">' . $index->imageTiny . '</a>' : $index->imageTiny;
  $output = '<div class="ad region"><small class="right">Advertisement</small>' . $output . '</div>';
  return $output;
}

// used for a contact forum
function __encryptTime($time, $decrypt = FALSE) {
  $secretNumber = '8370286762550';
  if (!$decrypt) {
    // encrypt
    $output = $time * $secretNumber - $secretNumber / 100;
  } else {
    // decrypt
    $output = ($time - $secretNumber / 100) / $secretNumber;
  }
  return $output;
}

// instead of rendering a menu drupal style, it renders menu in it's own style
function __renderMenuLi($menu, $newClass = NULL) {
  $menuArr = array();
  foreach($menu as $link) {
    $class = array();
    if ($link == reset($link)) {
      $class[] = 'first';
    }
    if ($link == end($link)) {
      $class[] = 'last';
    }
    $menuArr[] = '<li class="' . $newClass . '">' . l(check_plain($link['title']), $link['href'], $link) . '</li>';
  }
  
  $output = implode('', $menuArr);
  
  return $output;
}

// ?? need more info
// strips slashes from $_POST (query), to prevent MYSQL injections.
// used by contact form
//handle __POST(), including arrays type;
function __cleanPost() {

  $output = $_POST;
  
  //filter
  array_walk_recursive($output, function(&$n) {
    $n = trim(stripslashes($n));
  });
  
  return $output;
}

// TODO: Port into Core
// creates an Archive Menu, based on content
function __archiveMenu($nodeType) {
  $nid = ($node = menu_get_object()) ? $node->nid : NULL;

  /**
  * Create Archive Menu (array)
  */
  
  // get requested year and month
  $query = drupal_get_query_parameters();
  $requestedYear = isset($query['year']) && is_numeric($query['year']) ? $query['year'] : NULL;
  $requestedMonth = isset($query['month']) && is_numeric($query['month']) ? $query['month'] : NULL;
  
  // get last 5 years
  $thisYear = strftime('%Y');
  $yearOffset = 4;
  $allYears = range($thisYear, $thisYear - $yearOffset);
  
  // get available years; based on if posts exist for that year
  $query = NULL;
  foreach ($allYears as $year) {
    $startTime = gmmktime(0, 0, 0, 1, 1, $year);
    $endTime = gmmktime(0, 0, 0, 1, 1, $year + 1);
    // combine everything into a single request
    $query .= "(SELECT true FROM node WHERE status = 1 AND type = '" . $nodeType . "' AND created >= " . $startTime . " AND created < " . $endTime . " LIMIT 1) AS '" . $year . "'";
    if($year !== end($allYears)){
      $query .= ', ';
    }
  }
  $result = db_query('SELECT ' . $query);
  $result = $result->fetchAll();
  $availableYears = array();
  foreach ($result[0] as $key=>$val) {
    if ($val) { 
      $availableYears[] = $key;
    }
  }
  
  // get year to show
  $showYear = in_array($requestedYear, $availableYears) ? $requestedYear : $thisYear; // if $requestedYear not in $availableYears set $thisYear
  
  // define months
  $allMonths = array_reverse(range(1, 12));
  $monthNames = array(1 => t('January'), 2 => t('February'), 3 => t('March'), 4 => t('April'), 5 => t('May'), 6 => t('June'), 7 => t('July'), 8 => t('August'), 9 => t('September'), 10 => t('October'), 11 => t('November'), 12 => t('December'));

  $availableMonths = array();
  $showMonth = NULL;
  if ($showYear) {
    // get available months; based on if posts exist for that year
    $query = NULL;
    foreach($allMonths as $month) {
      $startTime = gmmktime(0, 0, 0, $month, 1, $showYear);
      $endTime = gmmktime(0, 0, 0, $month + 1, 1, $showYear);
      $query .= "(SELECT true FROM node WHERE type = '" . $nodeType . "' AND created >= " . $startTime . " AND created < " . $endTime . " LIMIT 1) AS '" . $month . "'";
      if($month !== end($allMonths)){
        $query .= ', ';
      }
    }
    $result = db_query('SELECT ' . $query);
    $result = $result->fetchAll();
    foreach ($result[0] as $key=>$val) {
      if ($val) { 
        $availableMonths[] = $key;
      }
    }
    
    // get month to show
    $showMonth = in_array($requestedMonth, $availableMonths) ? $requestedMonth : NULL; // if $requestedMonth not in $availableMonths do not set a month
  }
  
  // Render Menu
  $output = '<div class="archive region menu">';
  $output .= '<h3>Archive</h3>';
  $output .= '<ul class="year">';
  /**
  // create show all link
  $output .= '<li>';
  if (!$showYear) {
    $output .= '<a class="active" href="' . url('node/' . $nid) . '">' . t('Show All') . '</a>';
  } else {
    $output .= '<a href="' . url('node/' . $nid) . '">' . t('Show All') . '</a>';
  }
  $output .= '</li>';
  */
  // year
  foreach ($availableYears as $year) {
    if ($year === reset($availableYears)) {
      $output .= '<li class="first">';
    } else {
      $output .= '<li>';
    }
    if ($year == $showYear) {
      $output .= '<a class="active" href="' . url('node/' . $nid, array('query' => array('year' => $year))) . '">' . $year . ' & earlier</a>';
      //check this if statement
      if ($availableMonths) {
        $output .= '<ul class="month">';
        foreach($availableMonths as $month) {
          if ($month === reset($availableMonths)) {
            $output .= '<li class="first">';
          } else {
            $output .= '<li>';
          }
          if ($month == $showMonth) {
            $output .= '<a class="active" href="' . url('node/' . $nid, array('query' => array('year' => $year, 'month' => $month))) . '">' . $monthNames[$month] . '</a>';
          } else {
            $output .= '<a href="' . url('node/' . $nid, array('query' => array('year' => $year, 'month' => $month))) . '">' . $monthNames[$month] . '</a>';
          }
          $output .= '</li>';
        }
        $output .= '</ul>';
      }
    } else {
      $output .= '<a href="' . url('node/' . $nid, array('query' => array('year' => $year))) . '">' . $year . ' & earlier</a>';
    }
    $output .= '</li>';
  }
  $output .= '</ul>';
  $output .= '</div>';
  
  $menuOutput = $output;
  
  // get date range
  if ($showMonth) {
    $startTime = gmmktime(0, 0, 0, $showMonth, 1, $showYear);
    $endTime = gmmktime(0, 0, 0, $showMonth + 1, 1, $showYear);
  } else if ($showYear) {
    $startTime = gmmktime(0, 0, 0, 1, 1, $showYear);
    $endTime = gmmktime(0, 0, 0, 1, 1, $showYear + 1);
  } else {
    $startTime = 0;
    $endTime = mktime();  
  }

  $dateRange = array('start' => $startTime, 'end' => $endTime);
  
  $output = array('menu' => $menuOutput, 'dateRange' => $dateRange);
  
  return $output;
}

// TODO: Port into Core
// Renders featured content
function __featuredTeaser($nodeType, $options = NULL) {

  /**
  * API
  * 
  * __featuredRegion($nid[, array($limit=> '')]);
  */

  // get options
  $limit = isset($options['limit']) ? $options['limit'] : 10;
  
   //get nids for this page
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
  ->propertyCondition('type', $nodeType)
  ->propertyCondition('status', 1)
  ->fieldCondition('field_featured', 'value', 'featured')
  ->propertyOrderBy('created', 'DESC')
  ->range(0, $limit);
  $result = $query->execute();
  $nodes = isset($result['node']) ? $result['node'] : array();
  $nids = array();
  foreach ($nodes as $node) {
    $nids[] = $node->nid;
  }

  // Render Menu
  $output = '<div class="featured region menu">';
  $output .= '<h3>Featured</h3>';
  $output .= '<ul>';
  foreach ($nids as $nid) {
    $node = node_load($nid);
    $title = $node->title;
    $path = 'node/' . $nid;
    if ($nid === reset($nids)) {
      $output .= '<li class="first">';
    } else {
      $output .= '<li>';
    }
    $output .= l($title, $path);
    $output .= '</li>';
  }
  $output .= '</ul>';  
  $output .= '</div>';
  return $output;
} 

// TODO: Port into Core
// Renders featured content
function __teaser($nodeType, $count = 3) {

  // get Nids
  $options = array(
    'type' => $nodeType,
    'limit' => $count,
  );
  $nids = __getNids($options);

  // get teaser
  $output = __tease($nids);
  
  return $output;
} 

// TODO: Port into Core
//renders teaser of related content to the $nid supplied
function __relatedTeaser($nid, $options = NULL) {

  /**
  * API
  * 
  * __relatedRegion($nid[, array('taxonomyField' => '', 'imageField' => '', $limit=> '')]);
  */

  // get options
  $limit = isset($options['limit']) ? $options['limit'] : 3;
  $title = isset($options['title']) ? $options['title'] : 'Related';
  $more = isset($options['more']) ? $options['more'] : FALSE;

  $node = node_load($nid);

  $contentNid = $nid; //content nid
  $nids = array(); // nids to show
  
  /**
  * If $taxonomyField exists, get content related by tags (taxonomy field)
  */
  if (isset($node->field_tags)) {
    $vid = 5;
    
    //get related nids. Selects all nid which share the same taxonomy terms and arranges by frequency.
    //if (isset($node->{$termsField}[$node->language])) { $nodeTerms = $node->{$termsField}[$node->language]; } else { $nodeTerms = array(); }
    $result = db_query_range('SELECT nid FROM (SELECT nid, count(*) AS frequency FROM {taxonomy_index} WHERE tid IN (SELECT taxonomy_index.tid FROM {taxonomy_index} LEFT JOIN {taxonomy_term_data} ON taxonomy_index.tid = taxonomy_term_data.tid WHERE taxonomy_index.nid = :nid AND vid = :vid) AND nid != :nid GROUP BY nid ORDER BY frequency DESC) AS results', 0, $limit, array(':nid' => $nid, ':vid' => $vid));
    $result = $result->fetchAll();
    foreach ($result as $value) {
      $nids[] = $value->nid;
    }
  }
  
  /**
  * Get content related by date (80% older & 20% newer content, i.e. nearest siblings by date)
  */
  
  if (count($nids) < $limit) {
    // get limit
    $limit = max($limit - count($nids), 0); // get remaining limit
    $olderLimit = round($limit * 0.8); // olderLimit is 80% of remaining limit

    // define variables
    $nodeType = $node->type;
    $nodeCreated = $node->created;
    
    //load older nids
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
    //->propertyCondition('nid', array($nids), '!=')
    //->propertyCondition('nid', $contentNid, '!=')
    ->propertyCondition('type', $nodeType)
    ->propertyCondition('created', $nodeCreated, '<') 
    ->propertyCondition('status', 1)
    ->propertyOrderBy('created', 'DESC')
    ->range(0, $olderLimit);
    //exclude existing nids
    $query->propertyConditions[] = array('column'=>'nid', 'value'=>$contentNid, 'operator'=>'<>');
    foreach($nids as $nid) { $query->propertyConditions[] = array('column'=>'nid', 'value'=>$nid, 'operator'=>'<>'); }    
    //process query
    $result = $query->execute();
    if (isset($result['node'])) {
      $nodes = $result['node'];
      foreach ($nodes as $node) {
        $nids[] = $node->nid;
      }  
    }

    // get limit
    $newerLimit = max($limit - count($nids), 0); // get remaining limit
  
    //load newer nids
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
    //->propertyCondition('nid', array($nids), '!=')
    //->propertyCondition('nid', $contentNid, '!=')
    //->propertyConditions[] = $propertyConditions
    ->propertyCondition('type', $nodeType)
    ->propertyCondition('created', $nodeCreated, '>') 
    ->propertyCondition('status', 1)
    ->propertyOrderBy('created', 'ASC')
    ->range(0, $newerLimit);
    //exclude existing nids
    $query->propertyConditions[] = array('column'=>'nid', 'value'=>$contentNid, 'operator'=>'<>');
    foreach($nids as $nid) { $query->propertyConditions[] = array('column'=>'nid', 'value'=>$nid, 'operator'=>'<>'); }    
    //process query
    $result = $query->execute();
    if (isset($result['node'])) {
      $nodes = $result['node'];
      foreach ($nodes as $node) {
        $nids[] = $node->nid;
      }  
    }
  }
  
  $options = array(
    'name' => t('Related'),
  );
  
  $output = __tease($nids, $options);
  
  return $output;
}


/**
 * Implements hook_menu().
 */

/**
 * Implementation of hook_query_TAG_alter
 * This code allows to arrange EntityFieldQuery() by random
 */
/**
// don't know what this is for
function custom_functions_query_random_alter($query) {
  $query->orderRandom();
} 
*/