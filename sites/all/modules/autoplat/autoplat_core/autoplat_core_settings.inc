<?php

/**
 * Form builder function for module settings.
 */
function autoplat_core_settings() {

  $form['autoplat'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['autoplat_dev'] = array(
    '#type' => 'fieldset',
    '#title' => t('Developer'),
    '#group' => 'autoplat',
  );

  $devMode = variable_get('autoplat_settings_dev_status', FALSE);
  $form['autoplat_dev']['autoplat_settings_dev_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow override of disabled fields.'),
    '#description' => '<div class="messages warning">' . t('Caution! Enable only if you know what you\'re doing. Allows to alter disabled fields. Requires to manually regenerate urls, etc....') . '</div>',
    '#default_value' => $devMode,
  );

  return system_settings_form($form);
}