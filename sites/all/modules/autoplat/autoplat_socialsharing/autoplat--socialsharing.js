function parseData(data) {
  var items = data.split(';');
  items = items.filter(function (n) { return n != '' }); // filter out empty values
  var output = [];
  for (i = 0; i < items.length; i++) {
    var pos = items[i].indexOf(':');
    var item = [items[i].slice(0, pos), items[i].slice(pos + 1)];
    output[item[0]] = item[1];

  }

  return output;
}

function parseJsUrl(url) {
  // Get all instances of {x:x}
  // @see http://stackoverflow.com/questions/5520880
  var found = [],          // an array to collect the strings that are found
    rxp = /{([^}]+)}/g,
    url,
    curMatch;

  while (curMatch = rxp.exec(url)) {
    found.push(curMatch[1]);
  }

  // For each instance, get value
  var val = '';
  for (i2 = 0; i2 < found.length; i2++) {
    if (url.search('{' + found + '}') > 0) {
      var foundItems = found[i2].split('|');
      val = jQuery(foundItems[0]).attr(foundItems[1]); // Get contents of x:x, e.i. og:image.
      val = ( val ? val : '' ); // if val undefined (not found), set it to null
      url = url.replace('{' + found[i2] + '}', val); // update url
    }
  }

  return url;
}

function isNumber(o) {
  return !isNaN(o - 0) && o !== null && o !== "" && o !== false;
}

function formatCount(number) {
  var output = '';
  if (isNumber(number)) {
    output = number > 999 ? (number / 1000).toFixed() + 'k' : number;
  }
  return output;
}

function setFacebookCount(thisUrl) {
  jQuery.getJSON('http://graph.facebook.com/?id=' + thisUrl, function (data) {
    if (data.shares != null && data.shares > 0) {
      $('#autoplat-socialsharing a.facebook').append('<span class="count">' + formatCount(data.shares) + '</span>');
    }
  });
}

function setTwitterCount(thisUrl) {
  jQuery.getJSON('http://urls.api.twitter.com/1/urls/count.json?url=' + thisUrl + '&callback=?', function (data) {
    if (data.count != null && data.count > 0) {
      $('#autoplat-socialsharing a.twitter').append('<span class="count">' + formatCount(data.count) + '</span>');
    }
  });
}

function setPinterestCount(thisUrl) {
  jQuery.getJSON('http://widgets.pinterest.com/v1/urls/count.json?source=6&url=' + thisUrl + '&callback=?', function (data) {
    if (data.count != null && data.count > 0) {
      $('#autoplat-socialsharing a.pinterest').append('<span class="count">' + formatCount(data.count) + '</span>');
    }
  });
}

// Get shares count, i.e. likes/shares for Facebook
// View API docs on https://www.sharedcount.com/documentation.php
function setCount(type, thisUrl) {
  switch (type) {
    case 'facebook':
      setFacebookCount(thisUrl);
      break;
    case 'twitter':
      setTwitterCount(thisUrl);
      break;
    case 'pinterest':
      setPinterestCount(thisUrl);
      break;
    case 'googleplus':
      //setGoogleplusCount(thisUrl);  // not working yet, since requires api.
      break;
  }
}

(function ($) {
  Drupal.behaviors.myBehavior = {
    attach: function (context, settings) {
      $('a[data-autoplat-socialsharing]').each(function (e) {
        // On Click
        $(this).click(function (e) {
          e.preventDefault();
          var data = $(this).attr('data-autoplat-socialsharing');
          var items = parseData(data);
          var width = items['width'];
          var height = items['height'];
          var jsUrl = (items['jsUrl'] ? parseJsUrl(items['jsUrl']) : $(this).attr('href'));
          window.open(jsUrl, '_blank', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=' + width + ', height=' + height);
        });
        // Count likes
        $(this).append(function (e) {
          var data = $(this).attr('data-autoplat-socialsharing');
          var items = parseData(data);
          var type = items['type'];
          var thisUrl = items['thisUrl'];
          setCount(type, thisUrl);
          //return '<span class="count">'+count+'</span>';
        });
      });
    }
  };
})(jQuery);
