<?php
$output = '';
$networks = array('facebook', 'twitter', 'pinterest', 'googleplus');
$thisUrl = $content['#value']['#thisUrl'];
foreach ($networks as $network) {
  // load network values
  $network = !empty($content['#value'][$network]) ? $content['#value'][$network] : array();
  // if network values are loaded
  if (!empty($network)) {
    // Do not modify data-autoplat-socialsharing attribute
    $output .= '<a class="' . $network['machine_name'] . '" data-autoplat-socialsharing="type:' . $network['machine_name'] . ';thisUrl:' . $thisUrl . ';width:' . $network['popupWidth'] . ';height:' . $network['popupHeight'] . ';jsUrl:' . $network['jsUrlPrefix'] . $thisUrl . $network['jsUrlSuffix'] . ';" href="' . $network['urlPrefix'] . $thisUrl . $network['urlSuffix'] . '" title="' . $network['hoverTitle'] . '" target="' . $network['target'] . '"><span class="name">' . $network['title'] . '</span></a>';
  }
}

echo '<div id="autoplat-socialsharing" class="socialsharing"><div class="inner">' . $output . '</div></div>';