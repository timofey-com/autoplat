<?php

/**
 * Implements hook_install().
 */
function autoplat_pinboards_install() {

  $t = get_t();

  // Add the node type.
  $pages = array(
    'type' => 'autoplat_pinboards',
    'name' => $t('Pin Boards'),
    'base' => 'node_content',
    'description' => $t('Create groups of featured content. Can be used for banners, teasers, as highlights, etc...'),
    'title_label' => $t('Title'),
  );

  $content_type = node_type_set_defaults($pages);
  node_type_save($content_type);

  autoplat_pinboards_add_custom_fields();

  // Do not preview
  variable_set('node_preview_autoplat_pinboards', 0);

  // Set Node Options
  // +------------------------------------+
  // |   status (Published)               |
  // |   promote (Promoted to front page) |
  // |   sticky = Sticky at top of lists  |
  // | x revision = Crete new revision    |
  // +------------------------------------+
  variable_set('node_options_autoplat_pinboards', array('revision'));

  // Do not display author and date information
  variable_set('node_submitted_autoplat_pinboards', 0);

  // Show in Autoplat GUI
  variable_set('autoplat_gui_status_autoplat_pinboards', 1);

  // Enable Publish Content (Allows publish/unpublish with link)
  variable_set('publishcontent_autoplat_pinboards', 1);
}

/**
 * Implements hook_uninstall().
 */
function autoplat_pinboards_uninstall() {

  $type = 'autoplat_pinboards';
  $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
  $result = db_query($sql, array(':type' => $type));
  $nodeids = array();
  foreach ($result as $row) {
    $nodeids[] = $row->nid;
  }
  node_delete_multiple($nodeids);

  // should i remove this in favor of below? Primary problem was that this module was removing field autoplat_parent, which was used by autoplat_noderel
  node_type_delete($type);

  /**
   * $info = node_type_get_type($type);
   * db_delete('node_type')->condition('type', $type)->execute();
   * //field_attach_delete_bundle('node', $type);
   * module_invoke_all('node_type_delete', $info);
   */

  // Clear the node type cache.
  node_type_cache_reset();

  autoplat_pinboards_delete_custom_fields();
  field_purge_batch(500);
}

function autoplat_pinboards_add_custom_fields() {

  foreach (_autoplat_pinboards_installed_fields() as $field) {
    if (!field_info_field($field['field_name'])) {
      field_create_field($field);
    }
  }
  foreach (_autoplat_pinboards_installed_instances() as $fieldinstance) {
    $fieldinstance['entity_type'] = 'node';
    $fieldinstance['bundle'] = 'autoplat_pinboards';
    $fieldinstance['display']['default']['type'] = 'hidden';
    field_create_instance($fieldinstance);
  }
}

// Probably not used, since instances and fields seem to be deleted in node_type_delete();
// node_type_delete() seem to remove all fields of bundle content type.
function autoplat_pinboards_delete_custom_fields() {

  foreach (array_keys(_autoplat_pinboards_installed_fields()) as $field) {
    field_delete_field($field);
  }
  $instances = field_info_instances('node', 'autoplat_pinboards');
  foreach ($instances as $fieldinstance) {
    field_delete_instance($fieldinstance);
  }
}

/**
 * Adding fields to content type.
 */
function _autoplat_pinboards_installed_fields() {

  $t = get_t();
  return array(
    'autoplat_pinboards_uuids' => array(
      'field_name' => 'autoplat_pinboards_uuids',
      'label' => $t('Content'),
      'type' => 'text',
      'cardinality' => -1,
    ),
  );
}

function _autoplat_pinboards_installed_instances() {

  $t = get_t();
  return array(
    'autoplat_pinboards_uuids' => array(
      'field_name' => 'autoplat_pinboards_uuids',
      'label' => $t('Content'),
      'widget' => array(
        'type' => 'text_textfield'
      ),
      'description' => t('Content pinned to this board.'),
    ),
  );
}