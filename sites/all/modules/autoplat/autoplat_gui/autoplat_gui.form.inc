<?php

/**
 * @file
 * Example demonstrating a simple (i.e. 'sort' only) tabledrag form
 */

/**
 * Process array of forms
 */
function autoplat_gui_form_multiple($process, $items, $teaser = FALSE) {

  __adminRequired();

  $output = array();
  foreach ($items as $item) {
    $output[] = drupal_get_form($process, $item, $teaser);
  }

  return $output;
}

/**
 * Build the autoplat_gui_form form.
 *
 * @return array
 *
 * @ingroup tabledrag_example
 */
function autoplat_gui_form($form, &$form_state, $item, $teaser = FALSE) {

  __adminRequired();

  $fields = field_info_instances("node", $item['type']);

  $nodePriority = isset($fields['weight']) && variable_get('autoplat_nodePriority_status_' . $item['type'], FALSE) ? TRUE : FALSE;

  //full
  if (!$teaser) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->propertyCondition('type', $item['type'])
      ->pager($item['perPage']);
    // Sort by created date or weight.
    if ($nodePriority) $query->fieldOrderBy('weight', 'value', 'ASC');
    else $query->propertyOrderBy('created', 'DESC');
    $result = $query->execute();
  } //teaser
  else {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->propertyCondition('type', $item['type'])
      ->range(NULL, 5);
    // Sort by created date or weight.
    if ($nodePriority) $query->fieldOrderBy('weight', 'value', 'ASC');
    else $query->propertyOrderBy('created', 'DESC');
    $result = $query->execute();
  }

  $pager = theme('pager');

  $nids = array();
  if (isset($result['node'])) {
    foreach ($result['node'] as $node) {
      $nids[] = $node->nid;
    }
  }

  $nodes = node_load_multiple($nids);

  // pass form values
  $form['autoplat_gui_items']['#tree'] = TRUE;

  // If nodePriority is enabled & page is Full View.
  $draggable = !$teaser && $nodePriority ? TRUE : FALSE;
  $form['autoplat_gui_items']['#draggable'] = $draggable;

  // Form Header
  $addButton = user_access('create ' . $item['type'] . ' content') ? l('<i class="fa fa-plus"></i> Add', 'node/add/' . $item['path'], array(
    'attributes' => array('class' => array('button')),
    'query' => array('destination' => 'autoplat/' . $item['path']),
    'html' => TRUE
  )) : NULL;
  $value = variable_get('autoplat_parent_' . $item['type'], FALSE);
  $parents = $value ? autoplat_noderel_getParents($value) : array();
  $breadcrumbArray[] = l('Front', NULL);
  foreach ($parents as $nid) {
    $node = node_load($nid);
    $breadcrumbArray[] = l($node->title, 'node/' . $nid);
  }
  $breadcrumb = implode(' / ', $breadcrumbArray);
  if (!$teaser) {
    $form['#prefix'] = '<section class="header">' . $addButton . l('<i class="fa fa-chevron-up"></i> Overview', 'autoplat', array(
        'attributes' => array('class' => array('button')),
        'html' => TRUE,
      )) . $breadcrumb . '</section>';
  } else {
    $form['#prefix'] = '<h2>' . l($item['title'], 'autoplat/' . $item['path']) . '</h2><section class="header">' . $addButton . l('<i class="fa fa-chevron-right"></i> More', 'autoplat/' . $item['path'], array(
        'attributes' => array('class' => array('button')),
        'html' => TRUE,
      )) . $breadcrumb . '</section>';
  }

  // Form Footer
  $form['#suffix'] = !$teaser && isset($suffix) ? '<div>' . $suffix . '</div>' : NULL;
  $form['#suffix'] .= isset($pager) ? '<div class="pagination region">' . $pager . '</div>' : NULL;

  // Now we add our submit button, for submitting the form results.
  //
  // The 'actions' wrapper used here isn't strictly necessary for tabledrag,
  // but is included as a Form API recommended practice.
  if ($draggable) {
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  }

  // Add class to the form.
  $form['#attributes']['class'] = array('autoplat-form');


  // if not results, return FALSE;
  // TODO: work on it. FALSE puts out an error
  if (empty($result)) {
    $form['#prefix'] .= '<p>' . t('No items.') . '</p>';
    return $form;
}

  // fill form
  foreach ($nodes as $node) {

    // add column for drag
    $form['autoplat_gui_items'][$node->nid] = $draggable ? array('drag' => array('#markup' => NULL)) : array();

    //create fields

    //nid
    $nid = array(
      'nid' => array(
        '#markup' => $node->uuid,
        '#title' => t('Id')
      )
    );

    //title
    $title = array(
      'title' => array(
        '#markup' => __nodeTitleLink($node),
        '#title' => t('Title')
      )
    );

    //image
    $imageField = variable_get('autoplat_imageField_' . $item['type'], FALSE);
    if ($imageField) {
      $count = __nodeImageCount($node, $imageField);
      $image = $count ? array(
        'image' => array(
          '#markup' => '<a class="colorbox-load" href="' . __nodeImageLargePath($node, $imageField) . '">' . __nodeImageThumb($node, $imageField) . '</a>',
          '#title' => NULL
        )
      ) : array(
        'image' => array()
      );
    } else {
      $image = array();
    }

    //date
    $created = '<span class="created">' . __nodeDateCreatedAgo($node) . ' by ' . __nodeAuthorLink($node) . '</span>';
    $revised = $node->revision_uid ? ' <span class="revised">revised ' . __nodeDateRevisedAgo($node) . ' by ' . __nodeRevisorLink($node) . '</span>' : NULL;
    $date = array(
      'date' => array(
        '#markup' => $created . $revised,
        '#title' => t('Created')
      )
    );

    // edit
    $isAuthor = $GLOBALS['user']->uid == $node->uid ? TRUE : FALSE;
    // - get all permissions
    $canEditAnyOfType = user_access('edit any ' . $node->type . ' content') ? TRUE : FALSE;
    $canEditOwnOfType = user_access('edit own ' . $node->type . ' content') ? TRUE : FALSE;
    // - summarize permissions
    $canEdit = $canEditAnyOfType || ($canEditOwnOfType && $isAuthor) ? TRUE : FALSE;
    $edit = $canEdit ? array(
      'edit' => array(
        '#markup' => l('Edit', 'node/' . $node->nid . '/edit', array('query' => array('destination' => 'autoplat/' . $item['path']))),
        '#title' => '',
      )
    ) : array();

    // Status
    // - Check if Module is enabled and Content Type enabled Publish Content module.
    $enabled = variable_get('publishcontent_' . $node->type, FALSE);
    $canPublish = FALSE;
    $canUnpublish = FALSE;
    if ($enabled) {
      // - Get all permissions.
      $canPublishAny = user_access('publish any content') ? TRUE : FALSE;
      $canPublishEditable = user_access('publish editable content') ? TRUE : FALSE;
      $canPublishAnyOfType = user_access('publish any ' . $node->type . ' content') ? TRUE : FALSE;
      $canPublishOwnOfType = user_access('publish own ' . $node->type . ' content') ? TRUE : FALSE;
      $canPublishEditableOfType = user_access('publish editable ' . $node->type . ' content') ? TRUE : FALSE;
      $canUnpublishAny = user_access('unpublish any content') ? TRUE : FALSE;
      $canUnpublishEditable = user_access('unpublish editable content') ? TRUE : FALSE;
      $canUnpublishAnyOfType = user_access('unpublish any ' . $node->type . ' content') ? TRUE : FALSE;
      $canUnpublishOwnOfType = user_access('unpublish own ' . $node->type . ' content') ? TRUE : FALSE;
      $canUnpublishEditableOfType = user_access('unpublish editable ' . $node->type . ' content') ? TRUE : FALSE;
      // - Summarize permissions.
      $canPublish = $canPublishAny || ($canPublishEditable && $canEdit) || $canPublishAnyOfType || ($canPublishOwnOfType && $isAuthor) || ($canPublishEditableOfType && $canEdit) ? TRUE : FALSE;
      $canUnpublish = $canUnpublishAny || ($canUnpublishEditable && $canEdit) || $canUnpublishAnyOfType || ($canUnpublishOwnOfType && $isAuthor) || ($canUnpublishEditableOfType && $canEdit) ? TRUE : FALSE;
    }
    // - Render.
    // -- If permission ok, show status + link, else show status only.
    if ($node->status) $str = $canUnpublish ? l('<i class="fa fa-check-square-o"></i> On', 'node/' . $node->nid . '/unpublish/' . drupal_get_token(), array('html' => TRUE)) : '<i class="fa fa-check-square-o"></i> On';
    else $str = $canPublish ? l('<i class="fa fa-square-o"></i> Off', 'node/' . $node->nid . '/publish/' . drupal_get_token(), array('html' => TRUE)) : '<i class="fa fa-square-o"></i> Off';
    $class = $node->status ? 'published' : 'unpublished';
    $status = array(
      'status' => array(
        '#markup' => '<span class="' . $class . '">' . $str . '</span>',
        '#title' => t('Status'),
      ),
    );

    //weight
    // TODO is this ok if weight = array()??? or does it have to have a value?
    // $value = !empty($node->autoplat_weight['und'][0]['value']) ? $node->autoplat_weight['und'][0]['value'] : 0;
    $weight = $nodePriority ? array(
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $node->weight['und'][0]['value'],
        '#delta' => 100,
        '#title_display' => 'invisible',
      )
    ) : array();

    // format column fields based on content format type
    if ($item['type'] == 'autoplat_pages') {
      //make form
      $indexTemplate = array(
        'template' => array(
          '#markup' => 'node--' . $node->nid . '.tpl.php',
          '#title' => t('Template')
        )
      );
      $form['autoplat_gui_items'][$node->nid] += $nid + $title + $image + $status + $edit + $indexTemplate;
    } else {
      //make form
      $form['autoplat_gui_items'][$node->nid] += $nid + $title + $image + $date + $status + $edit;
      $suffix = 'Template: node--' . $node->type . '.tpl.php';
    }

    if ($draggable) $form['autoplat_gui_items'][$node->nid] += $weight;

  };

  return $form;
}

/**
 * Theme callback for the autoplat_gui_form form.
 *
 * The theme callback will format the $form data structure into a table and
 * add our tabledrag functionality.  (Note that drupal_add_tabledrag should be
 * called from the theme layer, and not from a form declaration.  This helps
 * keep template files clean and readable, and prevents tabledrag.js from
 * being added twice accidently.
 *
 * @return array
 *   The rendered tabledrag form
 *
 * @ingroup tabledrag_example
 */
function theme_autoplat_gui_form($variables) {

  $form = $variables['form'];

  // get row ids
  $header = array();
  $rows = array();
  $i = 0;
  foreach (element_children($form['autoplat_gui_items']) as $id) {

    // generate a row

    $i++;

    $form['autoplat_gui_items'][$id]['weight']['#attributes']['class'] = array('example-item-weight');

    // get field ids (fieldname)
    $row = array();
    foreach (element_children($form['autoplat_gui_items'][$id]) as $fieldname) {
      $row[] = array('data' => drupal_render($form['autoplat_gui_items'][$id][$fieldname]), 'class' => array($fieldname));
    }

    $rows[] = array(
      'data' => $row,
      'class' => array('draggable'),
    );

    // generate header
    if ($i == 1) {
      foreach (element_children($form['autoplat_gui_items'][$id]) as $fieldname) {
        // generate header
        $header[] = !empty($form['autoplat_gui_items'][$id][$fieldname]['#title']) ? array(
          'data' => $form['autoplat_gui_items'][$id][$fieldname]['#title'],
          'class' => array($fieldname)
        ) : array(
          'data' => NULL,
          'class' => array($fieldname)
        );
      }
    }
  }

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'example-items-table'),
  ));

  // Render submit button
  $output .= drupal_render_children($form);

  if ($form['autoplat_gui_items']['#draggable']) drupal_add_tabledrag('example-items-table', 'order', 'sibling', 'example-item-weight');

  $output = '<div class="autoplat-gui-table">' . $output . '</div>';

  return $output;
}

/**
 * Submit callback for the autoplat_gui_form form.
 *
 * Updates the 'weight' column for each element in our table, taking into
 * account that item's new order after the drag and drop actions have been
 * performed.
 *
 * @ingroup tabledrag_example
 */
function autoplat_gui_form_submit($form, &$form_state) {

  // Because the form elements were keyed with the item ids from the database,
  // we can simply iterate through the submitted values.
  foreach ($form_state['values']['autoplat_gui_items'] as $nid => $item) {
    $node = node_load($nid);
    $node->weight[$node->language][0]['value'] = $item['weight'];
    field_attach_update('node', $node);
  }
}
