<?php
/**
 * @file
 * Custom login page template
 *
 * @ingroup page
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $vars['language']; ?>" version="XHTML+RDFa 1.0">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title><?php print $vars['title']; ?></title>
  <?php print $vars['favicon']; ?>
  <link rel="stylesheet" type="text/css" href="<?php print $vars['css']; ?>">
  <link href='http://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
</head>
<body class="autoplat login">
  <div class="content">
    <?php echo $vars['messages']; ?>
    <?php
    // modify the login form.
    $form = $vars['content'];
    $form['name']['#prefix'] = '<div class="top">';
    $form['name']['#title_display'] = "attribute";
    $form['name']['#description'] = NULL;
    $form['name']['#attributes']['placeholder'] = t('Username');
    $form['pass']['#title_display'] = "attribute";
    $form['pass']['#description'] = NULL;
    $form['pass']['#attributes']['placeholder'] = t('Password');
    $form['pass']['#suffix'] = '</div>';
    $form['actions']['#prefix'] = '<div class="bottom">';
    $query = drupal_get_query_parameters();
    $destination = !empty($query['destination']) ? array('destination' => $query['destination']) : array();
    $form['actions']['submit']['#prefix'] = '<div class="right"><div class="inner">';
    $form['actions']['submit']['#value'] = 'Proceed to Autoplat';
    $form['actions']['submit']['#suffix'] = '</div></div><div class="left"><div class="inner"><div class="item forgot-password"><a href="' . url('user/password', array('query' => $destination)) . '">Forgot password?</a></div><div class="item front"><a href="' . url() . '"><i class="fa fa-home"></i>&nbsp; Front page</a></div></div></div>';
    $form['actions']['#suffix'] = '</div>';
    echo drupal_render($form);
    ?>
    <div class="branding">
      <?php echo l('<img src="' . $vars['logo'] . '" alt="" /><span>Version ' . $vars['autoplat_core_version'] . '</span>', 'http://autoplat.com', array('html' => TRUE)); ?>
      <span></span>
    </div>
    <?php
    ?>
  </div>
</body>
</html>